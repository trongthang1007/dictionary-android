package com.example.dictionaryapp.ui.flashcard;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import com.example.dictionaryapp.MainActivity;
import com.example.dictionaryapp.R;

import java.util.ArrayList;
import dbhelpers.DatabaseAccess;
import model.Word;


public class FlashCardFragment extends Fragment {
    public final static int REQUEST_SPEAK = 456;
    private ViewPager mVpFlashCard;
    private FlashCardFragmentAdapter adapter;
    private ArrayList<Word> mList;
    private int currentPage = 0;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_flashcard, container, false);
        mVpFlashCard = root.findViewById(R.id.vp_flash);
        DatabaseAccess dbAccess = DatabaseAccess.getInstance(getContext(), MainActivity.DATABASE_EN_VIE);
        mList = dbAccess.getListRandomForFlashCard(20);
        adapter = new FlashCardFragmentAdapter(getFragmentManager(), mList);
        mVpFlashCard.setAdapter(adapter);
        mVpFlashCard.setCurrentItem(currentPage);
        return root;
    }
}